A small PHP script that make a news list from a Mastodon RSS feed

# How to use

    <?php
      include('mastodon-news.php');
      $feedURL = 'https://mastodon.instance/users/username.rss';
      mastodonNews($feedURL);
    ?>

The function will list all the toots where `#news` is present.
You can add an event date by writing in the toot, a valid date on a new line. Supported date formats are the ones supported by [`strtotime`](https://php.net/manual/en/function.strtotime.php).

# License

[GPL v3](https://www.gnu.org/licenses/gpl.html)
