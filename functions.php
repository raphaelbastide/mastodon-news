<?php
function isNews($string, $newsHashtag){ // If contains #upcoming
  if (!empty(getHashtags($string))) {
    $hashtags = getHashtags($string);
    foreach ($hashtags as $hashtag) {
      if (strcasecmp($hashtag,$newsHashtag) == 0) { // Check if = #upcoming, case insensitive
        return $hashtag;
      }
    }
  }
}
function extractDate($string){ // If contains a date (strtotime format) on its own separate line
  $lines = preg_split("~<br(?: /)?>~", $string);
  foreach ($lines as $line) {
    if (isDate($line)) {
      return isDate($line);
    }
  }
}
function isDate($string){
  if(strtotime($string)){
      return strtotime($string);
     // return date('m.d.y', $t);
   }else{
     return false;
   }
}
function getHashtags($string) {
  $hashtags= FALSE;
  preg_match_all("/(#\w+)/u", $string, $matches);
  if ($matches) {
    $hashtagsArray = array_count_values($matches[0]);
    $hashtags = array_keys($hashtagsArray);
  }
  return $hashtags;
}

function substrwords($text, $maxchar, $end='...') {
  if (strlen($text) > $maxchar || $text == '') {
    $words = preg_split('/\s/', $text);
    $output = '';
    $i      = 0;
    while (1) {
      $length = strlen($output)+strlen($words[$i]);
      if ($length > $maxchar) {
          break;
      }
      else {
        $output .= " " . $words[$i];
        ++$i;
      }
    }
    $output .= $end;
  }
  else {
    $output = $text;
  }
  return $output;
}
