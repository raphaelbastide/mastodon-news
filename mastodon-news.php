<?php
include('functions.php');
function mastodonNews($feedURL){
  $now = time();
  $newsHashtag = "#news";
  $i = 0;
  $rss = simplexml_load_file($feedURL);
  foreach ($rss->channel->item as $item) {
    $description = $item->description;
    $pubdate = $item->pubDate;
    $description = strip_tags($description, '<img><a><p><br>'); // Remove all tags except…
    if (isNews($description, $newsHashtag)) {
      if (extractDate($description)) {
        $hasEventDate = true;
        $eventDate = extractDate($description);
      }else {
        $eventDate = $now;
      }
      $hash = isNews($description, $newsHashtag);
      $description = str_replace($hash,"",$description); // Remove the hash
      if ($now >= $eventDate) {
        echo "<section class='past' id='id-$i'>";
      }else {
        echo "<section id='id-$i'>";
      }
      echo "<p>".$description."</p>";
      echo '<p>Event date: '.date('l jS \of F Y', $eventDate).'</p>';
      if (isset($item->enclosure)) {
        $url = $item->enclosure['url'];
        if(@is_array(getimagesize($url))){ // test if image file
          echo "<img class='thumbnail' src='".$url."'>";
        }
      }
      echo "</section>";
    }
    $i++;
  }
}
