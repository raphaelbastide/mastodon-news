<?php

// header("Content-Type: application/rss+xml; charset=UTF-8");
header('Content-type: text/xml');
include('functions.php');
$rssfeed = '<?xml version="1.0" encoding="UTF-8"?>';
$rssfeed .= '<rss version="2.0">';
$rssfeed .= '<channel>';
$rssfeed .= '<title>Raphaël Bastide News</title>';
$rssfeed .= '<link>https://news.raphaelbastide.com/</link>';
$rssfeed .= '<description>New projects events, exhibitions…</description>';
$rssfeed .= '<language>en-us</language>';

$feedURL = 'https://mastodon.social/users/raphaelbastide.rss';
$now = time();
$newsHashtag = "#news";
$rss = simplexml_load_file($feedURL);
foreach ($rss->channel->item as $item) {
  $description = $item->description;
  $pubdate = $item->pubDate;
  $link = $item->link;
  $description = strip_tags($description, '<img><a><br>'); // Remove all tags except…
  if (isNews($description, $newsHashtag)) {
    if (extractDate($description)) {
      $hasEventDate = true;
      $eventDate = extractDate($description);
    }else {
      $eventDate = $now;
    }
    $hash = isNews($description, $newsHashtag);
    $description = str_replace($hash,"",$description); // Remove the hash
    if (isset($item->enclosure)) {
      $url = $item->enclosure['url'];
    }
    $rssfeed .= '<item>';
    $rssfeed .= '<title>' . substrwords($description,30) . '</title>';
    $rssfeed .= '<description>' . $description . '</description>';
    $rssfeed .= '<link>' . $link . '</link>';
    $rssfeed .= '<pubDate>' . $pubdate . '</pubDate>';
    $rssfeed .= '</item>';
  }
}
$rssfeed .= '</channel>';
$rssfeed .= '</rss>';

echo $rssfeed;
